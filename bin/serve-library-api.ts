import args from "./args";
import {connect as connectStan} from "node-nats-streaming";
import NATS from "nats";
import { LibraryReader } from '../lib/database/reader';
import uuidv4 from "uuid/v4";

import IORedis from "ioredis";

const redis = new IORedis({
    host: args.redisHost,
});

const natsClient = NATS.connect({
    url: args.natsUrl,
    encoding: "binary"
});

const connection = connectStan(args.stanCluster,  uuidv4(), { nc: natsClient});

import { libraryService } from "../lib/api/app";
const app = libraryService(connection, natsClient, new LibraryReader(redis));

const port = process.env.PORT || 3000;
app.listen(port, (): void => 
    console.log(`Server is listening on port ${port}.`)
);
