import argparse from "argparse";

const parser = new argparse.ArgumentParser({
    version: '0.0.1',
    addHelp:true,
    description: 'Library API'
});

parser.addArgument(
    [ '-n', '--natsUrl' ],
    {
        help: 'Nats URL to connect to'
    }
);

parser.addArgument(
    [ '-r', '--redisHost' ],
    {
        help: 'Redis host'
    }
);

parser.addArgument(
    [ '-sc', '--stanCluster' ],
    {
        help: 'NATS streaming cluster to connect to'
    }
);

export default parser.parseArgs();