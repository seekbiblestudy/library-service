import { connect as connectStan, Stan } from "node-nats-streaming";
import NATS from "nats";
import args from "./args";
import uuidv4 from "uuid/v4";


const natsClient = NATS.connect({
    url: args.natsUrl,
    encoding: "binary",
});


import { LibraryWriter } from '../lib/database/writer';
import { LibraryReader } from '../lib/database/reader';
import { WritingService } from "../lib/WritingService";

import IORedis from "ioredis";

const redis = new IORedis({
    host: args.redisHost,
});

const main = async function(): Promise<void> {
    const stanClient = await new Promise<Stan>((accept: Function): void =>{
        const c = connectStan(args.stanCluster, uuidv4(), { nc: natsClient});
        c.on('connect', (): void => {
            accept(c);
        });
    });

    const reader = new LibraryReader(redis);
    const writer = new LibraryWriter(redis, reader);
    const writingService = new WritingService(stanClient, natsClient, writer, reader);

    writingService.run();
};


main();

