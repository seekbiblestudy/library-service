import jsc, { suchthat }  from "jsverify";

import { librarySchema } from "./api/libraryScema";

import generate from "project-name-generator";

const accessEnum = jsc.oneof([jsc.constant("private"), jsc.constant("friends"), jsc.constant("public"), jsc.string]);

import uuidv4 from "uuid/v4";

var uuidArb = jsc.bless({
    generator: jsc.generator.bless(function (): string {
        return uuidv4();
    })
});

const randomName = jsc.bless({
    generator: jsc.generator.bless(function (count): string {
        return generate({
            words: Math.min(count, 5),
        }).spaced;
    })
});

export const randomLibrary = jsc.record({
    id: jsc.constant(""),
    title: jsc.string,
    access: accessEnum,
    permissions: jsc.record({
        owners: jsc.array(jsc.string),
        collaborators: jsc.array(jsc.string),
        viewers: jsc.array(jsc.string)
    })
});
  
export const randomValidLibrary = suchthat(jsc.record({
    id: jsc.constant(""),
    title: randomName,
    access: jsc.oneof([jsc.constant("private"), jsc.constant("friends"), jsc.constant("public")]),
    permissions: jsc.record({
        owners: jsc.nearray(uuidArb),
        collaborators: jsc.array(uuidArb),
        viewers: jsc.array(uuidArb)
    })
}), (v: unknown): boolean => {
    const { error } = librarySchema.validate(v);
    if (error) {
        return false;
    } else {

        return true;
    }
});

export const randomDomainAdd = jsc.record({
    libraryId: uuidArb,
    domain: jsc.oneof([jsc.constant("example.com"), jsc.constant("example.org"), jsc.constant("example.net")]),
});