import { Client } from "nats";
import { LibraryWriter } from "./database/writer";
import { LibraryReader } from "./database/reader";
import { Stan, Message, Subscription } from "node-nats-streaming";
import { LIBRARY_CREATE_REQUEST_EVENT_KEY, LibraryCreateRequestEvent, LibraryDeleteEvent, LIBRARY_DELETE_EVENT_KEY, LibraryAddWebsiteEvent, LIBRARY_ADD_WEBSITE_EVENT_KEY } from "./types/events";
import { NewLibraryKey, NewWebsiteKey } from "./types/api";

const QUEUE_NAME = "LibraryStateWriterV1";

export class WritingService {
    private subscriptions: Subscription[] = [];
    
    public constructor (private stan: Stan, private nats: Client, private writer: LibraryWriter, private reader: LibraryReader) {

    }

    public  run(): void{
        var opts = this.stan.subscriptionOptions();
        opts.setDeliverAllAvailable();
        opts.setDurableName(QUEUE_NAME);
        opts.setAckWait(3000);
        opts.setMaxInFlight(10);
        this.subscriptions = [
            this.stan.subscribe(LIBRARY_CREATE_REQUEST_EVENT_KEY, opts),
            this.stan.subscribe(LIBRARY_DELETE_EVENT_KEY, opts),
            this.stan.subscribe(LIBRARY_ADD_WEBSITE_EVENT_KEY, opts)
        ];

        for (const sub of this.subscriptions) {
            sub.on('message', async (msg: Message): Promise<void> => {
                this.messageHandler(msg);
            });
        }
    }

    private async messageHandler(msg: Message): Promise<void> {
        const content = JSON.parse(msg.getData().toString()) ;
        switch(msg.getSubject()) {
            case LIBRARY_CREATE_REQUEST_EVENT_KEY:
                return await this.handleCreate(content);

            case LIBRARY_DELETE_EVENT_KEY:
                return await this.handleDelete(content);

            case LIBRARY_ADD_WEBSITE_EVENT_KEY:
                return await this.handleWebsiteAdd(content);
            default:
                console.error("Received invalid message:", msg.getSubject());
        }
    }

    public stop(): void {
        for (const subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
        this.subscriptions = [];
    }

    private async handleCreate(content: LibraryCreateRequestEvent): Promise<void> {
        if (!content.spec.permissions) {
            content.spec.permissions = {
                owners: [],
                collaborators:[],
                viewers: [],
            }
        }
        await this.writer.saveLibrary({
            title: content.spec.title as string,
            id: content.libraryId,
            permissions: {
                owners: content.spec.permissions.owners || [],
                collaborators: content.spec.permissions.owners || [],
                viewers: content.spec.permissions.owners || [],
            }
        });
        this.nats.publish(NewLibraryKey + content.libraryId, "SUCCESS");
    }

    private async handleDelete(event: LibraryDeleteEvent): Promise<void> {
        await this.writer.deleteLibrary(event.libraryId);
    }

    private async handleWebsiteAdd(event: LibraryAddWebsiteEvent): Promise<void> {
        await this.writer.addWebsiteToLibrary(event.libraryId,  event.domain);
        this.nats.publish(NewWebsiteKey + event.libraryId, "SUCCESS");
    }
}