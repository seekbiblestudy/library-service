import 'mocha';
import { expect } from 'chai';

import jsc from "jsverify";

import {randomValidLibrary, randomDomainAdd} from "../librarySpec";

import IORedis from "ioredis";
import { LibraryWriter } from './writer';
import { LibraryReader } from './reader';

import uuidv4 from "uuid/v4";
import uuidv5 from "uuid/v5";
import { LibraryWebsite } from './schema';

describe("Database Library website", (): void =>{
    const redis = new IORedis();

    
    jsc.property("Create and read",  randomValidLibrary, randomDomainAdd, async function (lib, randomDomainAdd): Promise<boolean> {
        redis.flushall();
        const domainId = uuidv5(randomDomainAdd.domain, uuidv5.DNS);
        lib.id = uuidv4();
        const reader = new LibraryReader(redis);
        const writer = new LibraryWriter(redis, reader);

        await writer.saveLibrary(lib);
        await writer.addWebsiteToLibrary(lib.id, randomDomainAdd.domain);

        const res = await reader.getLibraryWebsites(lib.id);
        expect(res).to.contain(domainId);
        const v = await reader.getLibraryWebsite(lib.id, randomDomainAdd.domain);
        expect(v).not.to.equal(undefined)
        expect((v as LibraryWebsite).domain).to.equal(randomDomainAdd.domain);
        expect((v as LibraryWebsite).id).to.equal(domainId);
        return true;
    });

});