export const LIBRARIES_BY_ID = "libaries.";
export const LIBRARY_WEBSITES_BY_ID = "library-websites.";
export const LIBRARY_BY_OWNING_USER = "user-owning-libraries.";
export const LIBRARY_BY_COLLABORATING_USER = "user-collaborating-libraries.";
export const LIBRARY_BY_VIEWING_USER = "user-viewing-libraries.";
export const LIBRARY_WEBSITES = "libraries-websites.";
import { Permissions} from "../types/api"

export interface Library {
    id: string;
    title: string;
    permissions: Permissions;
}

export interface LibraryWebsite {
    domain: string;
    id: string;
    time: string;
}