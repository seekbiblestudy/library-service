import 'mocha';
import { expect } from 'chai';


import jsc from "jsverify";

import {randomValidLibrary} from "../librarySpec";
import IORedis from "ioredis";
import { LibraryWriter } from './writer';
import { LibraryReader } from './reader';

import uuidv4 from "uuid/v4";
import { Library } from './schema';

describe("Database Libraries", (): void => {
    const redis = new IORedis();

    
    jsc.property("Create and read",  randomValidLibrary, async function (lib): Promise<boolean> {
        redis.flushall();
        lib.id = uuidv4();
        const reader = new LibraryReader(redis);
        const writer = new LibraryWriter(redis, reader);

        await writer.saveLibrary(lib);
        const result = await reader.getLibrary(lib.id);
        expect(result).to.deep.equal(lib);
        for (const owner of lib.permissions.owners) {
            const idList = await reader.getUserOwnedLibraries(owner);
            expect(idList).to.contain(lib.id);
        }

        for (const collaborator of lib.permissions.collaborators) {
            const idList = await reader.getUserCollaboratingLibraries(collaborator);
            expect(idList).to.contain(lib.id);
        }
        return true;
    });

    jsc.property("Create and delete",  randomValidLibrary, {tests: 20}, async function (lib: Library): Promise<boolean> {
        redis.flushall();
        lib.id = uuidv4();
        const reader = new LibraryReader(redis);
        const writer = new LibraryWriter(redis, reader);
        await writer.saveLibrary(lib);
        await writer.deleteLibrary(lib.id);
        const result = await reader.getLibrary(lib.id);
        expect(result).to.not.deep.equal(lib);
        for (const owner of lib.permissions.owners) {
            const idList = await reader.getUserOwnedLibraries(owner);
            expect(idList).to.not.contain(lib.id);
        }

        for (const collaborator of lib.permissions.collaborators) {
            const idList = await reader.getUserCollaboratingLibraries(collaborator);
            expect(idList).to.not.contain(lib.id);
        }
        return true;
    });
});