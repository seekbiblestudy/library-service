// Listen to events
// Write to redis.

import ioredis from "ioredis";
import { Library, LibraryWebsite, LIBRARIES_BY_ID, LIBRARY_WEBSITES, LIBRARY_WEBSITES_BY_ID, LIBRARY_BY_VIEWING_USER, LIBRARY_BY_COLLABORATING_USER, LIBRARY_BY_OWNING_USER } from "./schema";

import uuidv5 from "uuid/v5";
import { LibraryReader } from "./reader";

async function pushId(client: ioredis.Redis, table: string, keyId: string, valueId: string): Promise<void> {
    await client.sadd(table + keyId, valueId);
}

async function removeId(client: ioredis.Redis, table: string, keyId: string, valueId: string): Promise<void> {
    await client.srem(table + keyId, valueId);
}

export class LibraryWriter {
    public constructor(private client: ioredis.Redis, private reader: LibraryReader) {}

    public async saveLibrary(lib: Library): Promise<void> {
        const content = JSON.stringify(lib);
        await this.client.set(LIBRARIES_BY_ID + lib.id, content);

        for (const id of (lib.permissions.owners || [])) {
            pushId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
            pushId(this.client, LIBRARY_BY_COLLABORATING_USER, id, lib.id);
            pushId(this.client, LIBRARY_BY_OWNING_USER, id, lib.id);
        }

        for (const id of (lib.permissions.collaborators || [])) {
            pushId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
            pushId(this.client, LIBRARY_BY_COLLABORATING_USER, id, lib.id);
        }

        for (const id of (lib.permissions.viewers || [])) {
            pushId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
        }
    }

    public async deleteLibrary(id: string): Promise<void> {
        const lib = await this.reader.getLibrary(id);
        if (lib === undefined) return;
        await this.client.del(LIBRARIES_BY_ID + id);
        for (const id of (lib.permissions.owners)) {
            removeId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
            removeId(this.client, LIBRARY_BY_COLLABORATING_USER, id, lib.id);
            removeId(this.client, LIBRARY_BY_OWNING_USER, id, lib.id);
        }

        for (const id of (lib.permissions.collaborators || [])) {
            removeId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
            removeId(this.client, LIBRARY_BY_COLLABORATING_USER, id, lib.id);
        }

        for (const id of (lib.permissions.viewers || [])) {
            removeId(this.client, LIBRARY_BY_VIEWING_USER, id, lib.id);
        }
    }

    // TODO Import

    public async addWebsiteToLibrary(lib: string, website: string): Promise<void> {
        const domainId = uuidv5(website, uuidv5.DNS);

        const newRecord: LibraryWebsite = {
            domain: website,
            id: domainId,
            time: "",
        }

        const content = JSON.stringify(newRecord);
        await this.client.set(LIBRARY_WEBSITES_BY_ID + lib + "." + domainId, content);
        pushId(this.client, LIBRARY_WEBSITES, lib, domainId);
    }

    public async removeWebsiteFromLibrary(lib: string, website: LibraryWebsite): Promise<void> {
        const domainId = uuidv5(website.domain, uuidv5.DNS);
        const content = JSON.stringify(website);
        await this.client.set(LIBRARIES_BY_ID + lib + "." + domainId, content);
        pushId(this.client, LIBRARY_WEBSITES, lib, domainId);
    }
}