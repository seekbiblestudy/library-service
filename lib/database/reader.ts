import ioredis from "ioredis";
import { Library, LibraryWebsite, LIBRARIES_BY_ID, LIBRARY_BY_OWNING_USER, LIBRARY_WEBSITES, LIBRARY_WEBSITES_BY_ID, LIBRARY_BY_COLLABORATING_USER, LIBRARY_BY_VIEWING_USER } from "./schema";

import uuidv5 from "uuid/v5";

export class LibraryReader {
    public constructor(private client: ioredis.Redis) {

    }

    public async getLibrary(id: string): Promise<Library | undefined> {
        const res = await this.client.get(LIBRARIES_BY_ID + id);
        if (res) {
            return JSON.parse(res) as Library;
        } else {
            return undefined;
        }
    }

    public async getUserOwnedLibraries(userid: string): Promise<string[]> {
        const res = await this.client.smembers(LIBRARY_BY_OWNING_USER + userid);
        if (!res) return [];
        return res;
    }

    public async getUserCollaboratingLibraries(userid: string): Promise<string[]> {
        const res = await this.client.smembers(LIBRARY_BY_COLLABORATING_USER + userid);
        if (!res) return [];
        return res;
    }

    public async getUserViewingLibraries(userid: string): Promise<string[]> {
        const res = await this.client.smembers(LIBRARY_BY_VIEWING_USER + userid);
        if (!res) return [];
        return res;
    }

    public async getLibraryWebsites(id: string): Promise<string[]> {
        const res = await this.client.smembers(LIBRARY_WEBSITES + id);
        if (!res) return [];
        return res;
    }

    public async getLibraryWebsite(library: string, domain: string): Promise<LibraryWebsite | undefined> {
        const websiteId = uuidv5(domain, uuidv5.DNS);
        const res = await this.client.get(LIBRARY_WEBSITES_BY_ID + library + "." + websiteId);
        if (res) {
            return JSON.parse(res) as LibraryWebsite;
        } else {
            console.log(res);
            return undefined;
        }
    }
}