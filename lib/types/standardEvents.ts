import uuidv1 from "uuid/v1";

export const newStandardEvent = (): StandardEvent => {
    return {
        uuid: uuidv1(),
        source: "services.api.v1",
        time: new Date().toISOString(),
    }
}

export interface StandardEvent {
    uuid: string; // Uniquely identifies the event UUIDv1.
    source: string; // The application generating the request.
    time: string; // Time of request.
}

// TODO User events.