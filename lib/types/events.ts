import { NewLibraryRequest } from "./api";
import { StandardEvent } from "./standardEvents";

export const LIBRARY_CREATE_REQUEST_EVENT_KEY = "library.create_request";
export const LIBRARY_ERROR_EVENT_KEY = "library.error";
export const LIBRARY_DELETE_EVENT_KEY = "library.delete";
export const LIBRARY_IMPORT_EVENT_KEY = "library.import";
export const LIBRARY_ADD_WEBSITE_EVENT_KEY = "library.add_website";

export enum LibraryErrorCode {
    DuplicateId = "DUPLICATED_ID",
}


export interface LibraryCreateRequestEvent extends StandardEvent {
    libraryId: string; // UUIDv4 id, can be rejected if duplicated.
    spec: NewLibraryRequest; // The spec of the new library to create.
}

export interface LibraryErrorEvent extends StandardEvent {
    reactingTo?: string; // The event reacting to, if any.
    code: LibraryErrorCode;
}

export interface LibraryDeleteEvent extends StandardEvent {
    libraryId: string;
}

export interface LibraryImportEvent extends StandardEvent {
    libraryId: string;
    sourceLibraryId: string;
}

export interface LibraryAddWebsiteEvent extends StandardEvent {
    libraryId: string;
    domain: string;
}
