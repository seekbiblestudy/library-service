import uuidv4 from "uuid/v4";

export const NewLibraryKey = "LibraryStateService.AcknowledgeNewLibrary:";
export const NewWebsiteKey = "LibraryStateService.AcknowledgeNewWebsite:";

export interface InternalErrorResponse {
    id: string;
}

export function internalErrorResponse(code: number, error: unknown): InternalErrorResponse {
    const id = uuidv4();
    console.error("ERROR-LIB" + code, id, error);
    return { id  };
}

export interface Permissions {
    /**
     *
     * @type {UserIds}
     * @memberof Permissions
     */
    owners: string[];
    /**
     *
     * @type {UserIds}
     * @memberof Permissions
     */
    collaborators: string[];
    /**
     *
     * @type {UserIds}
     * @memberof Permissions
     */
    viewers: string[];
}

export interface NewLibraryRequest {
    title: string;
    permissions: Permissions;
}

export interface NewLibraryResponse {
    id: string;
    title: string;
    permissions: Permissions;
}

export interface NewWebsiteResponse {
    domain: string;
    id: string;
    // TODO approved: boolean;
}
