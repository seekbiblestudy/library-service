import Joi from "@hapi/joi";

export const librarySchema = Joi.object().keys({
    title: Joi.string().min(1).max(100).required(),
    permissions: Joi.object().keys({
        owners: Joi.array().items(Joi.string()).min(1).required(),
        collaborators: Joi.array().items(Joi.string()),
        viewers: Joi.array().items(Joi.string()),
    }).required(),
    id: Joi.string().uuid().valid("").optional(),
    access: ["public", "private", "friends"],
});