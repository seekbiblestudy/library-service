import express from 'express';
import {
    LibraryCreateRequestEvent,
    LIBRARY_CREATE_REQUEST_EVENT_KEY,
    LibraryAddWebsiteEvent,
    LIBRARY_ADD_WEBSITE_EVENT_KEY
} from "../types/events";
import uuidv4 from "uuid/v4";
import uuidv5 from "uuid/v5";
import { librarySchema } from "./libraryScema";
import { Stan }  from "node-nats-streaming";
import { Client } from "nats";
import { LibraryReader } from '../database/reader';
import { NewLibraryRequest, NewWebsiteResponse, NewLibraryKey, NewWebsiteKey, NewLibraryResponse, internalErrorResponse } from '../types/api';
import { newStandardEvent } from '../types/standardEvents';


function sendNats(connection: Stan, key: string, data: unknown): Promise<string> {
    return new Promise((accept, reject): void => {
        connection.publish(key, JSON.stringify(data), (err, guid: string): void => {
            if (err) reject(err);
            accept(guid);
        });
    });
}

function request(connection: Client, key: string): Promise<string> {
    return new Promise((accept): void => {
        connection.requestOne(key, null, 1000, function(response: string): void {
            accept(response);
        });
    });
}

export function libraryService(events: Stan, natsClient: Client, libraryReader: LibraryReader): express.Application {
    const app = express();
    app.use(express.json());

    app.post('/', async (req: express.Request, res: express.Response): Promise<void> => {
        const input = req.body as NewLibraryRequest;
        const { error } = librarySchema.validate(input);
        if (error) {
            console.error(error.details)
            res.status(400).json({
                "invalid-keys": error.details.keys,
            });
            return;
        }
        const id = uuidv4();
        const libraryConfirmation = request(natsClient, NewLibraryKey + id);

        try {
            const msg: LibraryCreateRequestEvent = {
                spec: input,
                libraryId: id,
                ...newStandardEvent()
            };
            
            await sendNats(events, LIBRARY_CREATE_REQUEST_EVENT_KEY, msg);
            
        } catch (err) {
            res.status(500).json(internalErrorResponse(1000, err));
            return;
        }

        try {
            await libraryConfirmation;
        } catch(err) {
            res.status(500).json(internalErrorResponse(1001, err));
            return;
        }

        const libraryResponse: NewLibraryResponse = {
            id: id,
            permissions: input.permissions || { owners: [], collaborators: [], viewers: []},
            title: input.title as string,
        };

        res.status(200).json( libraryResponse);
    });

    app.get('/:library', async (req: express.Request, res: express.Response): Promise<void> => {
        const libraryId = req.params.library;
        const lib = await libraryReader.getLibrary(libraryId);
        if (lib === undefined) {
            res.status(404).json( {});
        }
        res.status(200).json( lib);
    });

    app.put('/:library/website/:domain', async (req: express.Request, res: express.Response): Promise<void> => {
        const id = uuidv5(req.params.domain, uuidv5.DNS);
        const createConfirmation = request(natsClient, NewWebsiteKey + id);

        try {
            const createEvent: LibraryAddWebsiteEvent = {
                domain: req.params.domain,
                libraryId: req.params.library,
                ...newStandardEvent()
            };
            await sendNats(events, LIBRARY_ADD_WEBSITE_EVENT_KEY, createEvent);
        } catch (err) {
            res.status(500).json(internalErrorResponse(1002, err));
            return;
        }

        try {
            await createConfirmation;
        } catch(err) {
            res.status(500).json(internalErrorResponse(1003, err));
            return;
        }

        const libraryResponse: NewWebsiteResponse = {
            id: id,
            domain: req.params.domain,
        };

        res.status(200).json(libraryResponse);
    });

    return app;
}
