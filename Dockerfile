FROM node:10 as APPBUILD
COPY lib/ /app/lib
COPY bin/ /app/bin
COPY package.json /app/
COPY package-lock.json /app/
COPY tsconfig.json /app
WORKDIR /app
RUN npm install && npm run build && rm -rf node_modules/
RUN npm install --only=prod
RUN rm -rf lib/
RUN rm -rf bin/

FROM node:10
COPY --from=APPBUILD /app /app 
EXPOSE 80
CMD ["node", "/app/dist/bin/library-state-writer.js"]